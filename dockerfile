FROM node:12.7-alpine AS builder
COPY . ./sampl-app
WORKDIR /sampl-app
RUN npm i
RUN $(npm bin)/ng build --prod

FROM nginx:1.17.1-alpine
COPY --from=builder /sampl-app/dist/sampl-app/ /usr/share/nginx/html